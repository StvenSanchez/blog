---
title: "Postgresql"
date: 2021-03-05T10:46:54-06:00
draft: false
image: "postgresql.png"

---


Postgresql es un sistema de gestión de base de datos relacional, es un sistema open source y gratuito, hoy en día ofrece una gran variedad de opciones avanzadas. 

Se puede usar en casi todos los sistemas operativos. 

Este proyecto, liderado por Michael Stonebraker, fue uno de los primeros intentos en implementar un motor de base de datos relacional.

## Instalación ubuntu  

```
$ sudo apt update
$ sudo apt install postgresql postgresql-contrib
 
```

postgres crea predeterminadamente una cuenta con el usuario postgres y una contraseña que es vacía. Para configurar esto tenemos que entrar a postgres.

```

$ sudo -i -u postgres
$ psql
 
```
Estos comandos te harán entrar a la línea de comandos de postgres, para salir tienes que digitar el siguiente comando.

```
 # \q
```

## configuración

Para cambiarle la contraseña al usuario postgres, hacemos lo siguiente, dentro de la terminal de comando de postgres.

```
 # alter user postgres with password 'password' o la que quieras.
```

También podemos crear y alterar los roles de nuestros usuarios, esto lo podemos lograr usando los siguientes comandos en la terminal de linux.

```
#create user usuario with password 'contraseña';

Alteral roles

#ALTER ROLE name  WITH option 


Las opciones pueden ser: 

    | SUPERUSER | NOSUPERUSER
    | CREATEDB | NOCREATEDB
    | CREATEROLE | NOCREATEROLE
    | CREATEUSER | NOCREATEUSER
    | INHERIT | NOINHERIT
    | LOGIN | NOLOGIN
    | CONNECTION LIMIT connlimit
    | [ ENCRYPTED | UNENCRYPTED ] PASSWORD 'password'
    | VALID UNTIL 'timestamp'

```


## PgAdmin

pgAdmin es una herramienta que podemos utilizar de forma más fácil para administrar nuestra bases de datos. Es una interfaz gráfica, lo cual hará que no estemos lidiando con la terminal en linux. 

Para instalar PgAdmin en nuestro computador necesitamos saber que hay 2 versiones que funcionan muy bien, PgAdmin3 y PgAdmin4. Para instalarla en ubuntu lo hacemos desde la terminal de comandos. 

PgAdmin3

```
$ sudo apt-get install pgAdmin3
```

PgAdmin4 

```

$ sudo apt install -y pgadmin4-web

```
Configurador de PgAdmin4 para Ubuntu. 

```

$ sudo /usr/pgadmin4/bin/setup-web.sh

```

Para entrar a PgAdmin4 web usaremos la url que nos da cuando escribimos el comando pasado.

