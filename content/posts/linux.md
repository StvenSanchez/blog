---
title: "Linux"
date: 2021-03-29T10:55:26-06:00
draft: false 
image: "linux-logo.png"
---

Linux está en todo, Probablemente estés usando linux en estos momentos y no te estás dando cuenta, pero, ¿qué es ?. Es bastante importante destacar que linux no es un sistema operativo. Linux es un kernel, el kernel es el encargado de hacer que las partes del hardware se comuniquen con las del software, vendría siendo el motor de nuestro computador, este kernel (motor) se puede usar para crear distintos sistemas operativos, llamemos a los sistemas operativos “autos”, podemos tener varios tipos de autos, pero con el mismo motor, esto es lo que pasa en el mundo de los sistemas operativos basados en el kernel de linux. 

## Distribuciones

Linux está basado en unix, a unix se le conoce como el padre de otros sistemas operativos, sistemas operativos como GNU/linux que fue el primer sistema operativo libre, también Mac os, bebian GNU/linux, ubuntu, android y muchos otros. Los sistemas operativos basados en linux se les conoce como distribuciones, a esto se refieren a que tipo de auto tienes. Detrás de estas distribuciones hay una gran comunidad o grandes compañías que se dedican a darles mantenimiento a las distribuciones. 

Si vienes de windows y quieres instalar una distribución de linux, te recomiendo que aprendas a usar la línea de comandos, yo aprendí a usarla a la mala, algo genial de las distribuciones de linux es que si aprendes lo básico en la terminal eso te va servir para casi todas las distribuciones basadas en linux, asi que podras pasar de distribuciones y probarlas todas si eso quieres. 
## Ubuntu en Windows 

Ahora ya no necesitas instalar una distribución de linux en tu ordenador para probar la línea de comandos de linux, lo puedes hacer descargando en la tienda de windows, tendrás que descargar ubuntu, esto no pondrá en contacto con el ecosistema de distribuciones de linux. 

![ubuntu](https://i.imgur.com/WnBDABX.jpg)

## Linux en servidores

Cuando hablamos de servidores sin duda linux es el rey, ofrece un mejor rendimiento, estabilidad y seguridad, todo esto es lo que hace a linux el mejor en servidores, ya sean para servidores que queremos montar en nuestro hogar o en la nube. Las distribuciones de servidores  se manejan de manera similar a las distros normales el  cambio no es  mucho, una de las distribuciones más conocidas es ubuntu server, también tenemos centOs y otras más.  


Así que si usas linux sin darte cuenta. 