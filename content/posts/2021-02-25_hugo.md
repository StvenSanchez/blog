---
title: "Hugo"
date: 2021-02-25T10:04:02-06:00
draft: false
image: "hugo.png"

---
Usé Hugo para crear esta página, es una herramienta bastante fácil usar, solo lo instale, elegí un tema y escrbí mi primer post. 

Hugo es un generador de sitios estáticos, escrito en GO.

Los sitos web estáticos son aquellos enfocados en mostrar una información permanente al visitante, este tipo de web son incapaces de soportar aplicaciones wed, como gestor de bases de datos, foros, consultas online o emails.

Este tipo de web son más que suficiente para crear blog como este, web informaticas, estas páginas web pueden mostar texto, imagenes e incluso videos, la navegación es por enlances.

Hugo es para personas que prefieren escribir en un editor de texto en lugar de un navegador, como en los manejadores de contenido tradicionales(por ejemplo, Wordpress)

En fin si queres hacer un blog personal solo tenes que instalar hugo en tu computador.

## Instalación 

Si quieres instalar Hugo en Linux este seria el comando.

```

$ sudo apt install hugo

```

Para crear un nuevo sitio solo necesitas este otro comando.

```

$ Hugo new mi-sitio

```
## Themes 
Despues solo tenés que eligir un tema, Hugo tiene muchos temas para elgir [Hugo temas](https://themes.gohugo.io/).

Luego de haberlo elegido, tenés la opción de descargarlo como archivo Zip, o lo podes clonar con git (esta es la mejor opción)

```

cd themes
git init 
git clone git@github.com:peaceiris/hugo-theme-iris.git

```
## Posts

Para escribir contenido

```

$ hugo new posts/mi-primer-posts.md

```

md es de markdown, es un lenguaje de marcado ligero que trata de conseguir la máxima legibilidad y facilidad de publicación, es fundamental de la web semántica.

Documentación de [Hugo](https://gohugo.io/getting-started/).

Seguimos en el dia 1.