---
title: "Phoenix"
date: 2021-03-23T11:28:11-06:00
draft: false
image: "Elixir-Phoenix.jpg"
---

Phoenix es un framework, escrito en elixir, en el cual vamos a poder crear aplicaciones web dinámicas, phoenix es para elixir lo que rails es para ruby, o como django es para python, solo que aun mejor, nos permite crear aplicaciones de manera más organizada lo que nos va permitir trabajar de manera un poco más eficiente. 

## Instalación 

Para instalar phoenix, necesitaremos tener instalado elixir y erlang, te dejo aquí como puedes instarlos [Elixir y Erlang](https://stevensanchez.dev/posts/2021-03-08_elixir/).

## Hex 

La instalación de phoenix sería de la siguiente manera. Primero tenemos que instalar el administrador de paquetes hex. 

```
$ mix local.hex
```

Comprobamos que tenemos instalado correctamente elixir y erlang. 

```

~$ elixir --version
Erlang/OTP 23 [erts-11.1.7] [source] [64-bit] [smp:8:8] [ds:8:8:10] [async-threads:1]

Elixir 1.11.2 (compiled with Erlang/OTP 23)

```
## Instalación Phoenix

Luego de comprobar que tenemos instalado elixir y erlang podemos instalar elixir phoenix. 

```

$ mix archive.install hex phx_new 1.5.8

```

Es importante decir que phoenix pide que tengamos instalado node js para poder compilar los assets de la aplicación, también usará npm para instalar dependencias lo cual hará que instalar node js no sea opcional. 
 
## Instalar nodejs 

```

sudo apt-get install -y nodejs

```

Así también tenemos que instalar postgresql, para instalarlo y saber un poco mas te dejo esto [Postgresql](https://stevensanchez.dev/posts/postgresql/). Phoenix va a configurar las aplicaciones para que lo usen de manera predeterminada, pero podemos usar otros gestores como MySQL o MSSQL. Phoenix utiliza otro paquete de elixir para comunicarse con las bases de datos, este paquete es ecto.

Luego de haber instalado todo, solo nos haría falta crear una aplicación en phoenix y ponerla a correr. 

```
$ mix phx.new hello 

```

Con esto creamos un nuevo proyecto. 

```
* creating hello/config/config.exs
* creating hello/config/dev.exs
* creating hello/config/prod.exs
* creating hello/config/prod.secret.exs
* creating hello/config/test.exs
* creating hello/lib/hello/application.ex
* creating hello/lib/hello.ex
* creating hello/lib/hello_web/channels/user_socket.ex
* creating hello/lib/hello_web/views/error_helpers.ex
* creating hello/lib/hello_web/views/error_view.ex
* creating hello/lib/hello_web/endpoint.ex
* creating hello/lib/hello_web/router.ex
* creating hello/lib/hello_web/telemetry.ex
* creating hello/lib/hello_web.ex
* creating hello/mix.exs
* creating hello/README.md
* creating hello/.formatter.exs
* creating hello/.gitignore
* creating hello/test/support/channel_case.ex
* creating hello/test/support/conn_case.ex
* creating hello/test/test_helper.exs
* creating hello/test/hello_web/views/error_view_test.exs
* creating hello/lib/hello/repo.ex
* creating hello/priv/repo/migrations/.formatter.exs
* creating hello/priv/repo/seeds.exs
* creating hello/test/support/data_case.ex
* creating hello/lib/hello_web/controllers/page_controller.ex
* creating hello/lib/hello_web/templates/layout/app.html.eex
* creating hello/lib/hello_web/templates/page/index.html.eex
* creating hello/lib/hello_web/views/layout_view.ex
* creating hello/lib/hello_web/views/page_view.ex
* creating hello/test/hello_web/controllers/page_controller_test.exs
* creating hello/test/hello_web/views/layout_view_test.exs
* creating hello/test/hello_web/views/page_view_test.exs
* creating hello/lib/hello_web/gettext.ex
* creating hello/priv/gettext/en/LC_MESSAGES/errors.po
* creating hello/priv/gettext/errors.pot
* creating hello/assets/webpack.config.js
* creating hello/assets/.babelrc
* creating hello/assets/js/app.js
* creating hello/assets/css/app.scss
* creating hello/assets/js/socket.js
* creating hello/assets/package.json
* creating hello/assets/static/favicon.ico
* creating hello/assets/css/phoenix.css
* creating hello/assets/static/images/phoenix.png
* creating hello/assets/static/robots.txt

Fetch and install dependencies? [Yn] 


```

Nos va pedir que si queremos instalar las dependencias, le decimos que si. 

``` 
* running mix deps.get
* running mix deps.compile
* running cd assets && npm install && node node_modules/webpack/bin/webpack.js --mode development

We are almost there! The following steps are missing:

    $ cd hello

Then configure your database in config/dev.exs and run:

    $ mix ecto.create

Start your Phoenix app with:

    $ mix phx.server

You can also run your app inside IEx (Interactive Elixir) as:

    $ iex -S mix phx.server

```

Para poder correr la aplicación tenemos que crear una base datos y para ello usamos ecto. 

```

$ mix ecto.create

Compiling 14 files (.ex)
Generated hello app
The database for Hello.Repo has been created


```

Para poder visualizar e iniciar nuestra aplicación usamos el comando: 

```
$ mix phx.server

```

![phoenix](https://i.imgur.com/su1IOsG.png)
